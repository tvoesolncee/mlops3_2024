FROM python:3.11-slim

COPY . /app

RUN apt-get update
RUN pip3 install /app/${REQUIREMENTS_PATH}