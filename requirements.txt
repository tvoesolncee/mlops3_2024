cfgv==3.4.0
distlib==0.3.8
filelock==3.13.3
identify==2.5.35
nodeenv==1.8.0
platformdirs==4.2.0
PyYAML==6.0.1
virtualenv==20.25.1
