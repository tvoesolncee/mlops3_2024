# MLOps3.0 project

Репозиторий для прохождения курса по MLOps


# Настройка окружения

## Сборка docker

1. При сборке в dev-режиме, поменять в файле ".env" путь до необходимых зависимостей:

```REQUIREMENTS_PATH = requirements_dev.txt```

При сборе в prod-режиме:

```REQUIREMENTS_PATH = requirements.txt```

1. Необходимо собрать образ с помощью команды: 

```docker build -t mlops .```

2. Запустить докер для дальнейшей работы: 

```docker run --rm -it --name mlops_container mlops:latest bash```